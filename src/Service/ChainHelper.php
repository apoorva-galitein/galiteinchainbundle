<?php

namespace Galitein\ChainBundle\Service;

class ChainHelper
{
    private array $commandsHierarchy = [];
    private array $commands = [];

    public function addCommand(string $command, ?string $parent): void
    {
        if (null === $parent && !isset($this->commandsHierarchy[$command])) {
            $this->commandsHierarchy[$command] = [];
        } else {
            $this->commandsHierarchy[$parent][] = $command;
        }

        $this->commands[] = $command;
    }

    public function getCommands(): array
    {
        return [
            'commandsHierarchy' => $this->commandsHierarchy,
            'commands' => $this->commands,
        ];
    }

    public function validate(string $command): bool
    {
        if (!in_array($command, $this->commands, true)) {
            return true;
        }

        return in_array($command, array_keys($this->commandsHierarchy));
    }
}
