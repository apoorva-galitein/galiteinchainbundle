<?php

namespace Galitein\ChainBundle\CompilerPass;

use Galitein\ChainBundle\Commands\BaseChainCommand;
use Galitein\ChainBundle\Service\ChainHelper;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ChainCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $chainServices = $container->findTaggedServiceIds('app.chain');
        $chainHelper = $container->getDefinition(ChainHelper::class);
        $availableChainCommand = [];
        foreach ($chainServices as $class => $chainService) {
            $serviceDefinition = $container->getDefinition($class);
            $command = $serviceDefinition->getProperties()['command'] ?? null;
            if (null === $command) {
                return throw new InvalidConfigurationException(
                    sprintf('The "command" property is required on %s to register a command as chain command', $class)
                );
            }

            $availableChainCommand[] = $command;
        }

        foreach ($chainServices as $class => $chainService) {
            $serviceDefinition = $container->getDefinition($class);
            $command = $serviceDefinition->getProperties()['command'] ?? null;
            $serviceDefinition->setArgument('$name', $command);
            $parent = $serviceDefinition->getProperties()['parent'] ?? null;
            if (null !== $parent && !in_array($parent, $availableChainCommand)) {
                return throw new InvalidConfigurationException(
                    sprintf('Defined chain parent %s not found', $parent)
                );
            }

            $chainHelper->addMethodCall('addCommand', [$command, $parent]);
        }
    }
}
