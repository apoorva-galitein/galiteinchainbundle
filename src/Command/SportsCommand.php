<?php

declare(strict_types=1);

namespace Galitein\ChainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SportsCommand extends Command
{
    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->success([sprintf('Command %s run successfully..!!', $this->getName())]);

        return 1;
    }
}
