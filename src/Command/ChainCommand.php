<?php

declare(strict_types=1);

namespace Galitein\ChainBundle\Command;

use Galitein\ChainBundle\Service\ChainHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChainCommand extends Command
{
    public function __construct(string $name = null, private ChainHelper $chainHelper)
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this->setName('chain:debug');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        dd($this->chainHelper->getCommands());
    }
}
