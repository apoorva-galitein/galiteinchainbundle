<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FootballCommandTest extends KernelTestCase
{
    /**
     * Test the execution of the command
     */
    public function testError()
    {
        $result = shell_exec("php bin/console chain:football");

        $this->assertStringContainsString('ERROR', $result);
    }
}
