<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SportsCommandTest extends KernelTestCase
{
    /**
     * Test the execution of the command
     */
    public function testSuccess()
    {
        $result = shell_exec("php bin/console chain:sports");

        $this->assertStringContainsString('success', $result);
    }
}
